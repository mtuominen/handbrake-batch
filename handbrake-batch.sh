#!/bin/sh
 
# This script converts any kind of video the HandBrake CLI can handle
# into an Apple TV 3 supported format.
# Other presets can be listed with 'HandBrakeCLI --preset-list'
# Usage:
# Running the script without any parameters just uses *all* files in the
# current directory and converts them, so make sure you only have video files
# in the directory you're in.
#
# You can specify an input and output directory if you want:
# 'script <INPUT> <OUTPUT>'
 
if [ ! -n "$1" ]; then
IN='.'
OUT='../converted'
else
IN=$1
OUT=$2
fi

PRESET="AppleTV 3"

cd "$IN"
for InputItem in *;do
HandBrakeCLI -i "$InputItem" -o "$OUT/${InputItem}.m4v" --preset="$PRESET"
done
